package com.zk;

import com.zk.filter.MyFilter2;
import com.zk.listener.MyListener2;
import com.zk.servlet.MyServlet1;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.*;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@ServletComponentScan
public class SpringbootSampleApplication {

    /**
     * 使用代码注册MyServlet1
     * @return
     */
    @Bean
	public ServletRegistrationBean servletRegistrationBean(){
        return new ServletRegistrationBean(new MyServlet1(),"/servlet1/*");
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean(){
        return new FilterRegistrationBean(new MyFilter2());
    }

    @Bean
    public ServletListenerRegistrationBean servletListenerRegistrationBean(){
        return new ServletListenerRegistrationBean(new MyListener2());
    }

    public static void main(String[] args) {
		SpringApplication.run(SpringbootSampleApplication.class, args);
	}
}
