package com.zk.HandlerInterceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * 扩展webmvc配置
 * Created by zhengkai on 2017/1/17.
 */
@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {
    /**
     * 自定义资源映射
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/pic/**").addResourceLocations("classpath:/pic/");
    }

    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链,addPathPatterns 用于添加拦截规则,excludePathPatterns 用户排除拦截
        registry.addInterceptor(new MyInterCeptor1()).addPathPatterns("/**");
        registry.addInterceptor(new MyInterCeptor2()).addPathPatterns("/**");
    }
}
