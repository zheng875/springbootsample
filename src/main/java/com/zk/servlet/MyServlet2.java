package com.zk.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 自定义Servlet2
 * Created by zhengkai on 2017/1/17.
 */
@WebServlet(urlPatterns = "/servlet2/*",description = "servlet2")
public class MyServlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("-------------<doGet>---------------");
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("-------------<doPost>---------------");
        PrintWriter out = resp.getWriter();
        out.println("success2");
    }
}
