package com.zk.service;

import com.zk.dao.UserDao;
import com.zk.domain.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhengkai on 2017/1/18.
 */
@Service
public class UserService{

    @Autowired
    private UserDao userDao;

    public List<UserInfo> findAll() {
        return userDao.findAll();
    }

    public UserInfo findOne(int id) {
        return userDao.findOne(id);
    }
}
