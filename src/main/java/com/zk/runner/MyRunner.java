package com.zk.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 启动服务加载数据
 * Created by zhengkai on 2017/1/17.
 */
@Component
@Order(value = 1)
public class MyRunner implements CommandLineRunner {
    @Override
    public void run(String... strings) throws Exception {
        System.out.println("服务启动执行，执行加载数据等操作");
    }
}
