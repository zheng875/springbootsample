package com.zk.controller;

import com.alibaba.fastjson.JSON;
import com.zk.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * 控制器
 * Created by zhengkai on 2017/1/17.
 */
@Controller
public class MainController {
    public static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/","/index"})
    public String index(ModelMap model, HttpSession session){
        model.put("name","zk");
        model.put("sessionId",session.getId());
        logger.info("success");
        return "index";
    }

    /**
     * 查询用户
     * @return
     */
    @ResponseBody
    @RequestMapping("/finduser")
    public String findUser() {
        return JSON.toJSONString(userService.findAll());
    }

    /**
     * findById
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/user/{id}")
    public String findById(@PathVariable int id) {
        return JSON.toJSONString(userService.findOne(id));
    }


}
